/*
	Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
	
*/

const getCube = Math.pow(2, 3);

	// Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	
let num = 2;
console.log(`The cube of ${num} is ${getCube}`);


/*
	Create a variable address with a value of an array containing details of an address.
*/

let address = [258, "Washington Ave NW", "California", 90011];

	// Destructure the array and print out a message with the full address using Template Literals.

const [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);


/*
	Create a variable animal with a value of an object data type with different animal details as it’s properties.
*/

let animal = {
	name: "Lolong",
	weight: 1075,
	length: "20 ft 3 in"
};

	// Destructure the object and print out a message with the details of the animal using Template Literals.

const { name, weight, length} = animal;
console.log(`${name} was a saltwater crocodile. He weighed at ${weight} kgs with a measurement of ${length}.`);


/*
	Create an array of numbers.
*/
 
 let numbers = [1, 2, 3, 4, 5];

 	// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach((number) => {
	console.log(number); 
})

	// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numbers.reduce((x, y) =>  (x + y));
console.log(reduceNumber);


/*
	Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
*/

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	};
};

	// Create/instantiate a new object from the class Dog and console log the object.
const newDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(newDog);